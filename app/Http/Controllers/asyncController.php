<?php

namespace App\Http\Controllers;

use App\log;
use App\patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Infusionsoft\Infusionsoft;


class asyncController extends Controller
{
    private $infusionsoft;

    public function __construct()
    {

    }

    // init all data from doctena
    public function async()
    {

        $response = Http::withHeaders([
            'token' => 'fbee36d3-cd28-439a-8458-246714570441',
            'email' => 'philippe@phare36.com',
            'Content-Type' => 'application/json'
        ])->withoutVerifying()->get('https://staging-secure.doctena.com/api/v1/patients',[
            'practiceId' => 94177,
            'max' => -1,
        ]);

       // dd($response->json());

        $jsonData = $response->json();
        $collect = collect($jsonData["patients"])->where('anonymized',false);




            $this->insert($collect);
            $logger = new log();
            $logger->lastupdated = $this->dateIncrement($collect[0]["lastUpdated"]);
            $logger->save();






        dd($collect);

    }


    // the crone job // last updated date
    public function updated(Request $request)
    {

        $logger = log::all()->first();

        $response = Http::withHeaders([
            'token' => 'fbee36d3-cd28-439a-8458-246714570441',
            'email' => 'philippe@phare36.com',
            'Content-Type' => 'application/json'
        ])->withoutVerifying()->get('https://staging-secure.doctena.com/api/v1/patients/watch',[
            'practiceId' => 94177,
            'max' => -1,
            'fromDate'=> $logger->lastupdated
        ]);



        $jsonData = $response->json();

       if( isset($jsonData["patients"]))
       {

           $collect = collect($jsonData["patients"])->where('anonymized',false);
           foreach ($collect as $item){
               $old = patient::where('eid','=',$item["eid"])->first();

               if(isset($old->eid))
               {


                   $old->firstname = $item["firstName"] ?? null;
                   $old->lastname = $item["lastName"] ?? null;
                   $old->mobile = $item["mobile"] ?? null;
                   $old->zip = $item["zip"] ?? null;
                   $old->countryisocode = $item["countryIsoCode"] ?? null;
                   $old->lastupdated = $item["lastUpdated"] ?? null;
                   $old->dateofbirth = $item["dateOfBirth"] ?? null;
                   $old->language = $item["language"] ?? null;
                   $old->street = $item["street"] ?? null;

                   $old->save();
                   //send updated to infusion


               }else{
                   $this->insert([$collect]);
               }
           }

           if(isset($collect[0])){
               $logger->lastupdated = $this->dateIncrement($collect[0]["lastUpdated"]);
               $logger->save();
               echo 'changing date';
           }

           dd($collect);
       }

        dd($jsonData);

    }


    public function insert($collect)
    {
        foreach ($collect as $item){
            DB::table('patients')->insert([
                'eid' => $item["eid"],
                'firstname' => $item["firstName"] ?? null,
                'lastname' => $item["lastName"] ?? null,
                'mobile' => $item["mobile"] ?? null,
                'zip' => $item["zip"] ?? null,
                'countryisocode' => $item["countryIsoCode"] ?? null,
                'lastupdated' => $item["lastUpdated"] ?? null,
                'dateofbirth' => $item["dateOfBirth"] ?? null,
                'language' => $item["language"] ?? null,
                'street' => $item["street"] ?? null,

            ]);

            // send new to infusion
         }
    }

    public function dateIncrement($item)
    {
        $tmp = explode(':',$item);

        $zone = substr($tmp[2], 2, strlen($tmp[2])-2);

        $str = (string)(((int)($tmp[2][0].$tmp[2][1]))+1);

        $str = count_chars($str) < 2 ? '0'.$str : $str;

        return $tmp[0].':'.$tmp[1].':'.$str.$zone;
    }

    public function infusion()
    {
        $this->infusionsoft = new Infusionsoft(array(
            'clientId'     => 'wv285',
            'clientSecret' => '56dfe09662b0ad522fe97f1dc1650170d1fccbcbfa2cb118691d57ada37922ed',
            'redirectUri'  => 'http://127.0.0.1:8000/infusion/all',

        ));
        $this->infusionsoft->setToken(@unserialize("G74LBGO85BpsfsqSQegjGT2lIyzY"));
        $this->infusionsoft->requestAccessToken('78hvekXD');
        $contacts = [];

        $contacts = $this->infusionsoft->contacts()->all();



        dd($contacts);


    }


}
