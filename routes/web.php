<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/async',"asyncController@async")->name('doctena-init');
Route::get('/async/update',"asyncController@updated")->name('doctena-update');
Route::get('/infusion/all',"asyncController@infusion")->middleware("web")->name('infudion-all');
